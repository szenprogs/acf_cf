<?php

	class CFAJAXSend {
		public $url = '';
		public $action = '';
		private $current_url = '';
		private $options;

		function __construct($action_name, $url = false, $opt) {
			if (empty($action_name)) return false;
			$this->options = $opt;
			$this->url = ($url) ? $url : plugin_dir_url( __FILE__ );
			$this->action = $action_name;

			$this->current_url = $_SERVER['HTTP_REFERER'];

			$this->init();
		}

		public function init() {
			add_action('wp_ajax_' . $this->action, array ($this, 'callback'));
			add_action('wp_ajax_nopriv_' . $this->action, array ($this, 'callback_nopriv'));
			add_action('wp_footer', array ($this, 'script'));
		}

		public function callback_nopriv() {
			$this->callback();
		}

		public function callback() {
			$output = array (
				'data'		=> $_REQUEST,
				'status'	=> 'success',
				'message'	=> 'Заявка успешно отправлена!',
			);
			if (isset($output['data']['phone']) && !empty($output['data']['phone'])) {
				$output['data']['phone'] = preg_replace('/\D+/', '', $output['data']['phone']);
				if ($output['data']['phone'] == '' || strlen($output['data']['phone']) < 6) {
					$output['status'] = 'error';
					$output['message'] = 'Неверный номер телефона';
					$this->output($output);
				}
			}
			if (isset($output['data']['email']) && !empty($output['data']['email'])) {
				if (preg_match('/^([a-zа-я0-9_\.-]+)@([a-zа-я0-9_\.-]+)\.([a-zа-я\.]{2,6})$/ui', $output['data']['email']) == 0) {
					$output['status'] = 'error';
					$output['message'] = 'Email нужно вводить в формате name@site.ru';
					$this->output($output);
				}
			}
			$headers[] = 'From: ' . $this->options['mailfrom'];
			if ($this->options['hidden']) $headers[] = 'Bcc: ' . $this->options['hidden'];
			$headers[] = 'content-type: text/html';
			$items = '';
			if (!empty($output['data']['name'])) $items .= '<p>Имя: <b>' . htmlspecialchars(trim($output['data']['name'])) . '</b></p>';
			if (!empty($output['data']['phone'])) $items .= '<p>Телефон: <b>' . htmlspecialchars(trim($output['data']['phone'])) . '</b></p>';
			if (!empty($output['data']['email'])) $items .= '<p>Почта: <b>' . htmlspecialchars(trim($output['data']['email'])) . '</b></p>';
			if (!empty($output['data']['city'])) $items .= '<p>Город: <b>' . htmlspecialchars(trim($output['data']['city'])) . '</b></p>';
			if (!empty($output['data']['text'])) $items .= '<p>Сообщение: <br>' . htmlspecialchars(trim($output['data']['text'])) . '</p>';
			$message = $this->options['text'];
			$message = str_replace("%sitename%", $_SERVER['HTTP_HOST'], $message);
			$message = str_replace("%formname%", htmlspecialchars(trim($output['data']['goal_desc'])), $message);
			$message = str_replace("%pageurl%", $this->current_url, $message);
			$message = str_replace("%items%", $items, $message);
			$message = str_replace("%date%", date('d.m.Y'), $message);
			$message = str_replace("%time%", date('H:j:s'), $message);
			$message = str_replace("%timezone%", date('P'), $message);
			if ($this->options['refinfo']) {
				include_once('refinfo.class.php');
				$refinfo = new RefInfo();
				$refinfo->init();
				$ricont = '';
				$ricont .= '<p>Дата первого контакта: <b>' . $refinfo->data->first_date . '</b></p>';
				$ricont .= '<p>Первый контакт: <b>' . $refinfo->data->first_ref . '</b></p>';
				$ricont .= '<p>Реферал: <b>' . $refinfo->data->referer . '</b></p>';
				$ricont .= '<p>Параметры: <b>' . $refinfo->data->get . '</b></p>';
				$ricont .= '<p>IP-адрес: <b>' . $refinfo->data->ip . '</b></p>';
				$ricont .= '<p>User agent: <b>' . $refinfo->data->ua . '</b></p>';
				$message = str_replace("%refinfo%", $ricont, $message);
			}
			wp_mail(
				$this->options['mailto'],
				str_replace('%sitename%', $_SERVER['HTTP_HOST'], $this->options['subject']),
				$message,
				$headers
			);

			$amocrm = $this->options['api']['amocrm'];
			if (!empty($amocrm['account']) && !empty($amocrm['login']) && !empty($amocrm['apikey'])) {
				include_once('amocrm.class.php');
				$amo = new AmoCRM(array (
					'account'	=>	$amocrm['account'],
					'login'		=>	$amocrm['login'],
					'apikey'	=>	$amocrm['apikey'],
					'cookie'	=>	$_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'cookie_amocrm.ini'
				));
				$amo->init();
				$amo->addField('name', empty($output['data']['name']) ? 'Имя не указано' : $output['data']['name']);
				$amo->addField('company_name', $_SERVER['HTTP_HOST']);
				if (isset($refinfo)) {
					$amo->addCustomFieldLeads('Реферал', $refinfo->data->referer);
					$amo->addCustomFieldLeads('Параметры', $refinfo->data->get);
				}
				$amo->addCustomFieldContacts('EMAIL', $output['data']['email']);
				$amo->addCustomFieldContacts('PHONE', $output['data']['phone']);
				$amo->sendLead('Первичный контакт', $output['data']['name'], 0);
				$amo->sendContact();
			}

			$this->output($output);
		}

		public function output($output) {
			echo(json_encode($output));
			wp_die();
		}

		public function script() {
			echo "\n<script type=\"text/javascript\">var contact_form_ajax_url = '" . admin_url('admin-ajax.php') . "?action=" . $this->action . "';</script>\n";
			wp_enqueue_script('contact-form-ajax', $this->url . 'assets/js/contact-form-ajax.js', array ('jquery', 'jquery-fancybox'), false, true);
		}
	}

?>