<?php

/*
	class WPPluginOptions
	===================================================
	Author:	Zagirskiy Ruslan Aleksandrovich
	Email:	szenprogs@gmail.com
	Skype:	szenprogs
	Site:	http://szenprogs.ru
	===================================================
	USING:
	---------------------------------------------------

	include_once('class/wppluginoptions.class.php');
	$options_obj = new WPPluginOptions();
	$options_obj->set_prefix('plugin-prefix', 'Options Title', 'Menu Title');

	$options_obj->add_section('section1', 'Section 1 Title', 'Section 1 Description');
	$options_obj->add_field('section1', 'field1', 'Field 1 Name:', 'Field 1 Description', array ('type' => 'text', 'value' => 'def_value', 'class' => 'regular-text'));
	$options_obj->add_field('section1', 'field2', 'Field 2 Name:', 'Field 2 Description', array ('type' => 'text', 'value' => 'def_value', 'class' => 'small-text'));

	$options_obj->add_section('section2', 'Section 2 Title', 'Section 2 Description');
	$options_obj->add_field('section2', 'field1', 'Field 1 Name:', 'Field 1 Description', array ('type' => 'textarea', 'class' => 'large-text code'));
	$options_obj->add_field('section2', 'field2', 'Field 2 Name:', 'Field 2 Description', array ('type' => 'text', 'value' => 'def_value', 'class' => 'tiny-text'));

	$options_obj->init();
	$options = $options_obj->value();
	var_dump($options);
*/

	class WPPluginOptions {
		private $options = array ();
		private $group = '';
		private $page = '';
		private $varname = '';
		private $sections_id = '';
		private $fields_id = '';
		private $title = '';
		private $menu_name = '';
		private $rights = '';

		public function set_prefix($prefix, $title = '', $menu = '', $rights = 'manage_options') {
			$method_name = __FUNCTION__;
			$this->die_if(
				empty($prefix),
				'Prefix required.',
				$method_name
			);

			$this->set_options(
				$prefix . '-group',
				$prefix . '-page',
				$prefix . '-option',
				($title) ? $title : 'Options for ' . $prefix,
				($menu) ? $menu : $prefix . ': Options',
				$prefix . '-section-',
				$prefix . '-field-',
				$rights
			);
		}

		public function set_options($group, $page, $varname, $title = '', $menu = '', $sections_id = '', $fields_id = '', $rights = 'manage_options') {
			$method_name = __FUNCTION__;
			$this->die_if(
				empty($group),
				'Group name required.',
				$method_name
			);
			$this->die_if(
				empty($page),
				'Page name required.',
				$method_name
			);
			$this->die_if(
				empty($varname),
				'Variable name required.',
				$method_name
			);

			$this->group = $group;
			$this->page = $page;
			$this->varname = $varname;
			$this->title = ($title) ? $title : $page;
			$this->menu_name = ($menu) ? $menu : $page;
			$this->sections_id = ($sections_id) ? $sections_id : $group . '-section-';
			$this->fields_id = ($fields_id) ? $fields_id : $group . '-field-';
			$this->rights = $rights;
		}

		function __construct() {
			//$this->init();
		}

		public function init() {
			add_action('admin_menu', array ($this, 'options'));
			add_action('admin_init', array ($this, 'options_settings'));
		}

		public function add_section($name, $title = '', $desc = '') {
			$method_name = __FUNCTION__;
			$this->die_if(
				empty($name),
				'Section name required.',
				$method_name
			);

			$this->options[$name] = array ();
			$this->options[$name]['title'] = ($title) ? $title : $name;
			$this->options[$name]['desc'] = ($desc) ? $desc : '';
			$this->options[$name]['fields'] = array ();
			return $name;
		}

		public function add_field($secid, $name, $title = '', $desc = '', $atts = array ()) {
			$method_name = __FUNCTION__;
			$this->die_if(
				empty($secid),
				'Section id required.',
				$method_name
			);
			$this->die_if(
				!isset($this->options[$secid]),
				'Section "' . $secid . '" not found.',
				$method_name
			);
			$this->die_if(
				empty($name),
				'Field name required.',
				$method_name
			);

			$fields = &$this->options[$secid]['fields'][$name];
			$fields['title'] = ($title) ? $title : $name;
			$fields['desc'] = ($desc) ? $desc : '';
			$fields['atts'] = $atts;
			return array (
				'section'	=> $secid,
				'field'		=> $name
			);
		}

		public function options() {
			add_options_page(
				$this->title,
				$this->menu_name,
				$this->rights,
				$this->page,
				array ($this, 'options_page')
			);
		}

		public function options_page() {
			?>
				<div class="wrap">
					<?php screen_icon(); ?>
					<h2><?php echo get_admin_page_title() ?></h2>
					<form action="options.php" method="POST">
						<?php
							settings_fields($this->group);
							do_settings_sections($this->page);
							submit_button();
						?>
					</form>
				</div>
			<?php
		}

		public function options_settings() {
			register_setting(
				$this->group,
				$this->varname
			);
			foreach ($this->options as $skey => $section) {
				add_settings_section(
					$this->sections_id . $skey,
					$section['title'],
					'',
					$this->page
				);
				foreach ($section['fields'] as $fkey => $field) {
					$field['atts']['name'] = $this->varname . '[' . $skey . '][' . $fkey . ']';
					add_settings_field(
						$this->fields_id . $skey . '_' . $fkey,
						$field['title'],
						array ($this, 'render_options_field'),
						$this->page,
						$this->sections_id . $skey,
						array (
							'atts'		=> $field['atts'],
							'section'	=> $skey,
							'field'		=> $fkey,
							'desc'		=> $field['desc'],
						)
					);
				}
			}
		}

		public function render_options_field($data) {
			$val = $this->value($data['section'], $data['field']);
			$output = '';
			switch ($data['atts']['type']) {
				case 'textarea';
					unset($data['atts']['value']);
					$output .= '<textarea ' . acf_esc_attr($data['atts']) . '>' . $val . '</textarea>';
					if ($data['desc']) $output .= '<p class="description">' . $data['desc'] . '</p>';
					break;
				case 'check';
				case 'checkbox';
					$data['atts']['value'] = '1';
					if ($val) $data['atts']['checked'] = 'checked';
					if ($data['desc']) $output .= '<label>';
					$output .= '<input ' . acf_esc_attr($data['atts']) . '>';
					if ($data['desc']) $output .= $data['desc'] . '</label>';
					break;
				case 'radio';
				case 'select';
				case 'options';
				default;
					$data['atts']['value'] = $val;
					$output .= '<input ' . acf_esc_attr($data['atts']) . '>';
					if ($data['desc']) $output .= '<p class="description">' . $data['desc'] . '</p>';
					break;
			}
			echo $output;
		}

		public function value($section = false, $field = false) {
			$opt = get_option($this->varname);
			if ($section) {
				if ($field) {
					$val = $opt[$section][$field];
					$def = $this->options[$section]['fields'][$field]['atts']['value'];
					if (!isset($val)) {
						$val = isset($def) ? $def : '';
					}
				} else {
					$val = array ();
					foreach ($this->options[$section]['fields'] as $fkey => $fld) {
						$val[$fkey] = $opt[$section][$fkey];
						$def = $fld['atts']['value'];
						if (!isset($val[$fkey])) {
							$val[$key] = isset($def) ? $def : '';
						}
					}
				}
			} else {
				$val = array ();
				foreach ($this->options as $skey => $sec) {
					$val[$skey] = array ();
					foreach ($sec['fields'] as $fkey => $fld) {
						$val[$skey][$fkey] = $opt[$skey][$fkey];
						$def = $fld['atts']['value'];
						if (!isset($val[$skey][$fkey])) {
							$val[$skey][$fkey] = isset($def) ? $def : '';
						}
					}
				}
			}
			return $val;
		}

		private function die_if($match, $message, $method_name) {
			if ($match) $this->die_mess($message, $method_name);
		}

		private function die_mess($message, $method_name = __FUNCTION__) {
			$trace_arr = debug_backtrace();
			foreach ($trace_arr as $item) {
				if ($item['function'] == $method_name) {
					$trace = $item;
					break;
				}
			}
			$message = '<pre>' . $message;
			if (isset($trace)) {
				$message .= '<br>Line: ' . $trace['line'];
				$message .= '<br>File: ' . $trace['file'];
			}
			$message .= '</pre>';
			wp_die($message);
		}
	}

?>