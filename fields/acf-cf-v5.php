<?php

// exit if accessed directly
if( ! defined( 'ABSPATH' ) ) exit;


// check if class already exists
if( !class_exists('acf_field_cf') ) :

	class acf_field_cf extends acf_field {

		function __construct($settings) {
			$this->name = 'cf';
			$this->label = __('Форма обратной связи', 'acf-cf');
			$this->category = 'basic';
			$this->defaults = array (
				'form_type'	=> 'FT_FORM',
				'desc_rows'	=> 6,
			);
			$this->l10n = array (
				'error'	=> __('Error! Please enter a higher value', 'acf-cf'),
			);
			$this->settings = $settings;

			// vars
			$url = $this->settings['url'];
			$version = $this->settings['version'];
			// register & include CSS
			wp_register_style( 'acf-forms-cf', "{$url}assets/css/forms.css");
			wp_enqueue_style('acf-forms-cf');
			add_action('wp_head', array ($this, 'yandex_metrika'));
			add_action('wp_head', array ($this, 'google_analytics'));

			parent::__construct();
		}

		public function yandex_metrika($id) {
			$id = $this->settings['options']['counters']['metrika_id'];
			if (!$id) return false;
			echo '
			<!-- Yandex.Metrika counter -->
			<script type="text/javascript">
				(function (d, w, c) {
					(w[c] = w[c] || []).push(function() {
						try {
							w.yaCounter = new Ya.Metrika({
								id:' . $id . ',
								clickmap:true,
								trackLinks:true,
								accurateTrackBounce:true,
								webvisor:true
							});
						} catch(e) { }
					});
					var n = d.getElementsByTagName("script")[0],
						s = d.createElement("script"),
						f = function () { n.parentNode.insertBefore(s, n); };
					s.type = "text/javascript";
					s.async = true;
					s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";
					if (w.opera == "[object Opera]") {
						d.addEventListener("DOMContentLoaded", f, false);
					} else { f(); }
				})(document, window, "yandex_metrika_callbacks");
			</script>
			<noscript><div><img src="//mc.yandex.ru/watch/' . $id . '" style="position:absolute; left:-9999px;" alt=""></div></noscript>
			<!-- /Yandex.Metrika counter -->
			';
		}

		public function google_analytics() {
			$id = $this->settings['options']['counters']['analytic_id'];
			if (!$id) return false;
			echo '
			<!-- GoogleAnalytics counter -->
			<script type="text/javascript">
				(function(i,s,o,g,r,a,m) {
					i.GoogleAnalyticsObject = r;
					i[r] = i[r] || function() {
						(i[r].q = i[r].q || []).push(arguments);
					};
					i[r].l = 1 * new Date();
					a = s.createElement(o);
					m = s.getElementsByTagName(o)[0];
					a.async = 1;
					a.src = g;
					m.parentNode.insertBefore(a,m);
				})(window, document, "script", "//www.google-analytics.com/analytics.js", "ga");
				ga("create", "' . $id . '", "auto");
				ga("require", "displayfeatures");
				ga("send", "pageview");

				/* Accurate bounce rate by time */
				if (!document.referrer || document.referrer.split("/")[2].indexOf(location.hostname) != 0) {
					setTimeout(function(){
						ga("send", "event", "Новый посетитель", location.pathname);
					}, 15000);
				}
			</script>
			<!-- /GoogleAnalytics counter -->
			';
		}

		public function render_field_settings( $field ) {
			acf_render_field_setting( $field, array (
				'label'			=> __('Вывод формы','acf-cf'),
				'instructions'	=> __('В каком виде будет отображаться форма','acf-cf'),
				'type'			=> 'radio',
				'name'			=> 'form_type',
				'layout'		=> 'vertical',
				'choices'		=> array (
					'FT_FORM'	=> __('Форма', 'acf-cf'),
					'FT_BUTTON'	=> __('Кнопка', 'acf-cf'),
				),
			));

			acf_render_field_setting( $field, array (
				'label'			=> __('Количество строк в описании формы', 'acf-cf'),
				'instructions'	=> __('Сколько строк будет в описании формы', 'acf-cf'),
				'type'			=> 'number',
				'name'			=> 'desc_rows',
				'placeholder'	=> 6,
			));

			//================================================================================

			acf_render_field_setting( $field, array (
				'label'			=> __('<span style="color:green;">Умолчание:</span><br> Текст кнопки', 'acf-cf'),
				'type'			=> 'text',
				'name'			=> 'def_button_text',
			));

			acf_render_field_setting( $field, array (
				'label'			=> __('<span style="color:green;">Умолчание:</span><br> Заголовок формы', 'acf-cf'),
				'type'			=> 'text',
				'name'			=> 'def_form_title',
			));

			acf_render_field_setting( $field, array (
				'label'			=> __('<span style="color:green;">Умолчание:</span><br> Текст формы', 'acf-cf'),
				'type'			=> 'textarea',
				'name'			=> 'def_form_desc',
			));

			acf_render_field_setting( $field, array (
				'label'			=> __('<span style="color:green;">Умолчание:</span><br> Текст на кнопке формы', 'acf-cf'),
				'type'			=> 'text',
				'name'			=> 'def_form_button',
			));

			acf_render_field_setting( $field, array (
				'label'			=> __('<span style="color:green;">Умолчание:</span><br> Название формы', 'acf-cf'),
				'type'			=> 'text',
				'name'			=> 'def_goal_desc',
			));

			acf_render_field_setting( $field, array (
				'label'			=> __('<span style="color:green;">Умолчание:</span><br> Группа цели', 'acf-cf'),
				'type'			=> 'text',
				'name'			=> 'def_goal_group',
			));

			acf_render_field_setting( $field, array (
				'label'			=> __('<span style="color:green;">Умолчание:</span><br> Название цели', 'acf-cf'),
				'type'			=> 'text',
				'name'			=> 'def_goal_name',
			));

			//================================================================================

			acf_render_field_setting( $field, array (
				'label'			=> __('<span style="color:green;background:#ccc;">Умолчание:</span><br> Введите ваше имя (Использовать)', 'acf-cf'),
				'type'			=> 'true_false',
				'name'			=> 'def_items_name_active',
			));

			acf_render_field_setting( $field, array (
				'label'			=> __('<span style="color:green;background:#ccc;">Умолчание:</span><br> Введите ваше имя (Обязательно)', 'acf-cf'),
				'type'			=> 'true_false',
				'name'			=> 'def_items_name_require',
			));

			acf_render_field_setting( $field, array (
				'label'			=> __('<span style="color:green;background:#ccc;">Умолчание:</span><br> Введите ваше имя (Подпись)', 'acf-cf'),
				'type'			=> 'text',
				'name'			=> 'def_items_name_title',
			));

			acf_render_field_setting( $field, array (
				'label'			=> __('<span style="color:green;background:#ccc;">Умолчание:</span><br> Введите ваше имя (Подпись в поле)', 'acf-cf'),
				'type'			=> 'text',
				'name'			=> 'def_items_name_placeholder',
			));

			//--------------------------------------------------------------------------------

			acf_render_field_setting( $field, array (
				'label'			=> __('<span style="color:green;">Умолчание:</span><br> Введите ваш телефон (Использовать)', 'acf-cf'),
				'type'			=> 'true_false',
				'name'			=> 'def_items_phone_active',
			));

			acf_render_field_setting( $field, array (
				'label'			=> __('<span style="color:green;">Умолчание:</span><br> Введите ваш телефон (Обязательно)', 'acf-cf'),
				'type'			=> 'true_false',
				'name'			=> 'def_items_phone_require',
			));

			acf_render_field_setting( $field, array (
				'label'			=> __('<span style="color:green;">Умолчание:</span><br> Введите ваш телефон (Подпись)', 'acf-cf'),
				'type'			=> 'text',
				'name'			=> 'def_items_phone_title',
			));

			acf_render_field_setting( $field, array (
				'label'			=> __('<span style="color:green;">Умолчание:</span><br> Введите ваш телефон (Подпись в поле)', 'acf-cf'),
				'type'			=> 'text',
				'name'			=> 'def_items_phone_placeholder',
			));

			//--------------------------------------------------------------------------------

			acf_render_field_setting( $field, array (
				'label'			=> __('<span style="color:green;background:#ccc;">Умолчание:</span><br> Введите вашу почту (Использовать)', 'acf-cf'),
				'type'			=> 'true_false',
				'name'			=> 'def_items_email_active',
			));

			acf_render_field_setting( $field, array (
				'label'			=> __('<span style="color:green;background:#ccc;">Умолчание:</span><br> Введите вашу почту (Обязательно)', 'acf-cf'),
				'type'			=> 'true_false',
				'name'			=> 'def_items_email_require',
			));

			acf_render_field_setting( $field, array (
				'label'			=> __('<span style="color:green;background:#ccc;">Умолчание:</span><br> Введите вашу почту (Подпись)', 'acf-cf'),
				'type'			=> 'text',
				'name'			=> 'def_items_email_title',
			));

			acf_render_field_setting( $field, array (
				'label'			=> __('<span style="color:green;background:#ccc;">Умолчание:</span><br> Введите вашу почту (Подпись в поле)', 'acf-cf'),
				'type'			=> 'text',
				'name'			=> 'def_items_email_placeholder',
			));

			//--------------------------------------------------------------------------------

			acf_render_field_setting( $field, array (
				'label'			=> __('<span style="color:green;">Умолчание:</span><br> Введите ваш город (Использовать)', 'acf-cf'),
				'type'			=> 'true_false',
				'name'			=> 'def_items_city_active',
			));

			acf_render_field_setting( $field, array (
				'label'			=> __('<span style="color:green;">Умолчание:</span><br> Введите ваш город (Обязательно)', 'acf-cf'),
				'type'			=> 'true_false',
				'name'			=> 'def_items_city_require',
			));

			acf_render_field_setting( $field, array (
				'label'			=> __('<span style="color:green;">Умолчание:</span><br> Введите ваш город (Подпись)', 'acf-cf'),
				'type'			=> 'text',
				'name'			=> 'def_items_city_title',
			));

			acf_render_field_setting( $field, array (
				'label'			=> __('<span style="color:green;">Умолчание:</span><br> Введите ваш город (Подпись в поле)', 'acf-cf'),
				'type'			=> 'text',
				'name'			=> 'def_items_city_placeholder',
			));

			//--------------------------------------------------------------------------------

			acf_render_field_setting( $field, array (
				'label'			=> __('<span style="color:green;background:#ccc;">Умолчание:</span><br> Введите ваше сообщение (Использовать)', 'acf-cf'),
				'type'			=> 'true_false',
				'name'			=> 'def_items_text_active',
			));

			acf_render_field_setting( $field, array (
				'label'			=> __('<span style="color:green;background:#ccc;">Умолчание:</span><br> Введите ваше сообщение (Обязательно)', 'acf-cf'),
				'type'			=> 'true_false',
				'name'			=> 'def_items_text_require',
			));

			acf_render_field_setting( $field, array (
				'label'			=> __('<span style="color:green;background:#ccc;">Умолчание:</span><br> Введите ваше сообщение (Подпись)', 'acf-cf'),
				'type'			=> 'text',
				'name'			=> 'def_items_text_title',
			));

			acf_render_field_setting( $field, array (
				'label'			=> __('<span style="color:green;background:#ccc;">Умолчание:</span><br> Введите ваше сообщение (Подпись в поле)', 'acf-cf'),
				'type'			=> 'text',
				'name'			=> 'def_items_text_placeholder',
			));
		}

		function render_field( $field ) {
			$field['value'] = acf_get_array ($field['value'], false);

			$atts = array (
				'button_text'	=> array (
					'type'			=> 'text',
					'value'			=> '',
				),
				'form_title'	=> array (
					'type'			=> 'text',
					'value'			=> '',
				),
				'form_desc'	=> array (
					'rows'			=> $field['desc_rows'],
				),
				'form_button'	=> array (
					'type'			=> 'text',
					'value'			=> '',
				),
				'goal_group'	=> array (
					'type'			=> 'text',
					'placeholder'	=> 'form',
					'value'			=> '',
				),
				'goal_name'		=> array (
					'type'			=> 'text',
					'placeholder'	=> 'goal_',
					'value'			=> '',
				),
				'goal_desc'		=> array (
					'type'			=> 'text',
					'value'			=> '',
				),
			);
			foreach ($atts as $key => &$att) {
				$att['id'] = $field['id'] . '-' . $key;
				$att['class'] = $key;
				$att['name'] = $field['name'] . '[' . $key . ']';
				if (isset($att['value'])) {
					if (!$att['value']) {
						if (isset($field['value'][$key])) {
							$att['value'] = $field['value'][$key];
						} else {
							$att['value'] = $field['def_' . $key];
						}
					}
				}
			}

			$output = '<div style="background:#eee;padding:10px;">';

			if ($field['form_type'] == 'FT_BUTTON') {
				$output .= '<hr><div><div class="acf-label"><label>Текст кнопки:</label></div><div class="acf-input"><input ' . acf_esc_attr($atts['button_text']) . '></div></div>';
				$output .= '<hr><div><div class="acf-label"><label>Заголовок формы:</label></div><div class="acf-input"><input ' . acf_esc_attr($atts['form_title']) . '></div></div>';
			}
			$output .= '<hr><div><div class="acf-label"><label>Текст формы:</label></div><div class="acf-input"><textarea ' . acf_esc_attr($atts['form_desc']) . '>' . esc_textarea(isset($field['value']['form_desc']) ? $field['value']['form_desc'] : $field['def_form_desc']) . '</textarea></div></div>';
			$output .= '<hr><div><div class="acf-label"><label>Текст на кнопке формы:</label></div><div class="acf-input"><input ' . acf_esc_attr($atts['form_button']) . '></div></div>';

			$items_atts = array (
				'name'			=> (object) array (
					'description'	=> 'Введите ваше имя:',
					'title'			=> array (),
					'placeholder'	=> array (),
					'active'		=> array (),
					'require'		=> array (),
					'type'			=> array ('value' => 'text'),
				),
				'phone'			=> (object) array (
					'description'	=> 'Введите ваш телефон:',
					'title'			=> array (),
					'placeholder'	=> array (),
					'active'		=> array (),
					'require'		=> array (),
					'type'			=> array ('value' => 'phone'),
				),
				'email'			=> (object) array (
					'description'	=> 'Введите вашу почту:',
					'title'			=> array (),
					'placeholder'	=> array (),
					'active'		=> array (),
					'require'		=> array (),
					'type'			=> array ('value' => 'email'),
				),
				'city'			=> (object) array (
					'description'	=> 'Введите ваш город:',
					'title'			=> array (),
					'placeholder'	=> array (),
					'active'		=> array (),
					'require'		=> array (),
					'type'			=> array ('value' => 'text'),
				),
				'text'			=> (object) array (
					'description'	=> 'Введите ваше сообщение:',
					'title'			=> array (),
					'placeholder'	=> array (),
					'active'		=> array (),
					'require'		=> array (),
					'type'			=> array ('value' => 'textarea'),
				),
			);
			$output .= '<hr><div><div class="acf-label"><label>Поля формы:</label></div><div class="acf-input"><table>';
			$output .= '<thead><tr><td>Поле</td><td>Использовать</td><td>Обязательно</td><td>Подпись</td><td>Подпись в поле</td></tr></thead>';
			$output .= '<tbody>';
			foreach ($items_atts as $key => &$item) {
				$item->title['name'] = $field['name'] . '[items][' . $key . '][title]';
				$item->title['type'] = 'text';
				$val = $field['value']['items'][$key]['title'];
				if (isset($val)) {
					$item->title['value'] = $val;
				} else {
					$item->title['value'] = $field['def_items_' . $key . '_title'];
				}

				$item->placeholder['name'] = $field['name'] . '[items][' . $key . '][placeholder]';
				$item->placeholder['type'] = 'text';
				$val = $field['value']['items'][$key]['placeholder'];
				if (isset($val)) {
					$item->placeholder['value'] = $val;
				} else {
					$item->placeholder['value'] = $field['def_items_' . $key . '_placeholder'];
				}

				$item->active['name'] = $field['name'] . '[items][' . $key . '][active]';
				$item->active['type'] = 'checkbox';
				$val = $field['value']['items'][$key]['active'];
				if (isset($val)) {
					if ($val == 'on') $item->active['checked'] = 'checked';
				} else {
					if ($field['def_items_' . $key . '_active']) $item->active['checked'] = 'checked';
				}

				$item->require['name'] = $field['name'] . '[items][' . $key . '][require]';
				$item->require['type'] = 'checkbox';
				$val = $field['value']['items'][$key]['require'];
				if (isset($val)) {
					if ($val == 'on') $item->require['checked'] = 'checked';
				} else {
					if ($field['def_items_' . $key . '_require']) $item->require['checked'] = 'checked';
				}

				$item->type['name'] = $field['name'] . '[items][' . $key . '][type]';
				$item->type['type'] = 'hidden';

				$output .= '<tr>';
				$output .= '<td>' . $item->description . '</td>';
				$output .= '<td><input ' . acf_esc_attr($item->active) . '></td>';
				$output .= '<td><input ' . acf_esc_attr($item->require) . '></td>';
				$output .= '<td><input ' . acf_esc_attr($item->title) . '></td>';
				$output .= '<td><input ' . acf_esc_attr($item->placeholder) . '><input ' . acf_esc_attr($item->type) . '></td>';
				$output .= '</tr>';
			}
			$output .= '</tbody></table></div></div><hr>';

			$output .= '<div style="background:#fff;padding:10px;">';
			$output .= '<div><div class="acf-label"><label>Название формы:</label></div><div class="acf-input"><input ' . acf_esc_attr($atts['goal_desc']) . '></div></div>';
			$output .= '<hr><div><div class="acf-label"><label>Группа цели:</label></div><div class="acf-input"><input ' . acf_esc_attr($atts['goal_group']) . '></div></div>';
			$output .= '<hr><div><div class="acf-label"><label>Название цели:</label></div><div class="acf-input"><input ' . acf_esc_attr($atts['goal_name']) . '></div></div>';
			$output .= '</div>';
			$output .= '</div>';

			echo $output;
		}

		function format_value( $value, $post_id, $field ) {
			if( empty($value) ) {
				return $value;
			}
			if (empty($field['form_width'])) $field['form_width'] = '400';

			$output = '';
			if ($field['form_type'] == 'FT_BUTTON') {
				$output .= '<div class="contact_form_button_wrap"><a href="#' . $field['key'] . '" class="contact_form_button_popup"><span>' . $value['button_text'] . '</span></a></div>';
				$output .= '<div style="display:none;">';
				$output .= '<div class="contact_form_popup" id="' . $field['key'] . '">';
				if ($value['form_title']) $output .= '<div class="contact_form_title">' . $value['form_title'] . '</div>';
			}
			$output .= '<div class="contact_form"><div class="contact_form_wrapper">';
			if ($value['form_desc']) $output .= '<div class="contact_form_description">' . preg_replace('/\n/i', '<br>', $value['form_desc']) . '</div>';
			$output .= '<form method="post" action="">';
			foreach ($value['items'] as $key => $item) {
				if (isset($item['active']) && $item['active'] == 'on') {
					$req = '';
					$atts = array ();
					$atts['name'] = $key;
					$atts['type'] = $item['type'];
					$atts['placeholder'] = $item['placeholder'];
					if (isset($item['require']) && $item['require'] == 'on') {
						$atts['required'] = 'required';
						$req = ' <sup title="' . __('Обязательно к вводу', 'acf-cf') . '">*</sup>';
					}
					$output .= '<div class="contact_form_item">';
					if ($item['title']) $output .= '<label>' . $item['title'] . $req . '</label>';
					$output .= '<div><input ' . acf_esc_attr($atts) . '></div>';
					$output .= '</div>';
				}
			}

			$atts = array (
				'type'	=> 'hidden',
				'name'	=> 'goal_group',
				'value'	=> $value['goal_group'],
			);
			$output .= '<input ' . acf_esc_attr($atts) . '>';
			$atts = array (
				'type'	=> 'hidden',
				'name'	=> 'goal_name',
				'value'	=> $value['goal_name'],
			);
			$output .= '<input ' . acf_esc_attr($atts) . '>';
			$atts = array (
				'type'	=> 'hidden',
				'name'	=> 'goal_desc',
				'value'	=> $value['goal_desc'],
			);
			$output .= '<input ' . acf_esc_attr($atts) . '>';

			$output .= '<div class="contact_form_button"><button type="submit"><span>' . $value['form_button'] . '</span></button></div>';
			$output .= '</form>';
			$output .= '<div class="contact_form_error" style="display:none;"></div>';
			$output .= __('<div class="contact_form_politic">Ваши данные под надежной защитой политики кофиденциальности</div>', 'acf-cf');
			$output .= '</div>';
			if ($field['form_type'] == 'FT_BUTTON') {
				$output .= '<a class="contact_form_close" href="#close">Close</a>';
				$output .= '</div>';
				$output .= '</div>';
			}
			$output .= '</div>';


			return $output;
		}

	}


	// initialize
	new acf_field_cf( $this->settings );


	// class_exists check
endif;

?>