<?php

/*
Plugin Name: Advanced Custom Fields: Contact Form Field
Plugin URI: http://szenprogs.ru
Description: Contact Form Field for ACF
Version: 1.1.3
Author: Szen
Author URI: http://szenprogs.ru
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
*/

	if( ! defined( 'ABSPATH' ) ) exit;
	if( !class_exists('acf_plugin_cf') ) :
		if (!class_exists('WPPluginOptions')) include_once(plugin_dir_path( __FILE__ ) . 'options.php');
		if ($options['mail']['refinfo']) {
			if (!class_exists('RefInfo')) include_once(plugin_dir_path( __FILE__ ) . 'class/refinfo.class.php');
			$refinfo = new RefInfo();
			$refinfo->init();
		}

		class acf_plugin_cf {
			function __construct() {
				global $options;
				$this->settings = array(
					'version'	=> '1.0.0',
					'url'		=> plugin_dir_url( __FILE__ ),
					'path'		=> plugin_dir_path( __FILE__ ),
					'options'	=> $options,
				);
				load_plugin_textdomain( 'acf-cf', false, plugin_basename( dirname( __FILE__ ) ) . '/lang' );
				add_action('acf/include_field_types', 	array ($this, 'include_field_types')); // v5
				add_action('acf/register_fields', 		array ($this, 'include_field_types')); // v4
			}

			function include_field_types( $version = false ) {
				if( !$version ) $version = 4;
				include_once('fields/acf-cf-v' . $version . '.php');
			}
		}
		new acf_plugin_cf();

		if (!class_exists('CFAJAXSend')) include_once('class/CFAJAXSend.php');
		$mail_opt = $options['mail'];
		$mail_opt['api']['amocrm'] = $options['amocrm'];
		new CFAJAXSend('contact_form_send', plugin_dir_url( __FILE__ ), $mail_opt);
	endif;

?>