<?php

	include_once('class/wppluginoptions.class.php');
	$acf_cf_opt = new WPPluginOptions();

	$acf_cf_opt->set_options(
		'acf-cf-grop',
		'acf-cf-option-page',
		'acf_cf_option',
		'Настройки формы обратной связи (ACF)',
		'ACF: Форма обратной связи',
		'acf_cf_section_',
		'acf_cf_field_',
		'manage_options'
	);


	// SECTION mail
	$acf_cf_opt->add_section('mail', 'Отправка заявок', 'Параметры отправки заявок с сайта');
	$acf_cf_opt->add_field('mail', 'subject', 'Заголовок письма:',
		'Текст <code>%sitename%</code> заменяется доменом сайта',
		array (
			'type'		=> 'text',
			'value'		=> 'Заявка с сайта %sitename%',
			'class'		=> 'regular-text',
		)
	);
	$acf_cf_opt->add_field('mail', 'mailfrom', 'От кого:',
		'Только одна электронная почта',
		array (
			'type'		=> 'text',
			'value'		=> 'Site Mailer <no-reply@mymail.com>',
			'class'		=> 'regular-text',
		)
	);
	$acf_cf_opt->add_field('mail', 'mailto', 'Адресат заявок:',
		'Можно поставить несколько почтовых ящиков, прописывать через запятые',
		array (
			'type'		=> 'text',
			'class'		=> 'regular-text',
		)
	);
	$acf_cf_opt->add_field('mail', 'hidden', 'Скрытые адресаты:',
		'',
		array (
			'type'		=> 'text',
			'class'		=> 'regular-text',
		)
	);
	$acf_cf_opt->add_field('mail', 'text', 'Текст сообщения:',
		'Возможны подстановки: <ul><li><code>%sitename%</code> - домен сайта</li><li><code>%pageurl%</code> - страница, с которой пришла заявка</li><li><code>%date%</code> - дата заявки</li><li><code>%time%</code> - время заявки</li><li><code>%timezone%</code> - временная зона заявки</li><li><code>%formname%</code> - имя формы</li><li><code>%items%</code> - отправленные данные</li><li><code>%refinfo%</code> - реферальные хвосты</li></ul>',
		array (
			'type'		=> 'textarea',
			'value'		=> "<h1>Заявка с сайта %sitename%</h1>\n<hr>\n<h2>Веденные данные:</h2>\n%items%\n<hr>\n<p>Название формы: <b>%formname%</b></p>\n<p>Страница: <b>%pageurl%</b>\n<p>Дата заявки: <b>%date% %time% %timezone%</b></p>\n<hr>\n<h2>Реферальные хвосты:</h2>\n%refinfo%",
			'cols'		=> 90,
			'rows'		=> 12,
			'class'		=> 'regular-text code'
		)
	);
	$acf_cf_opt->add_field('mail', 'refinfo', 'Реф хвосты:',
		'Отправлять реферальные хвосты',
		array (
			'type'		=> 'checkbox',
		)
	);

	// SECTION counters
	$acf_cf_opt->add_section('counters', 'Настройки счетчиков', 'Параметры счетчиков статистики');
	$acf_cf_opt->add_field('counters', 'metrika_id', 'Яндекс Metrica ID:',
		'',
		array (
			'type'		=> 'text',
		)
	);
	$acf_cf_opt->add_field('counters', 'analytic_id', 'Google Analitycs ID:',
		'',
		array (
			'type'		=> 'text',
		)
	);

	// SECTION amocrm
	$acf_cf_opt->add_section('amocrm', 'amoCRM', 'Параметры интеграции с amoCRM');
	$acf_cf_opt->add_field('amocrm', 'account', 'Аккаунт:',
		'',
		array (
			'type'		=> 'text',
		)
	);
	$acf_cf_opt->add_field('amocrm', 'login', 'Логин:',
		'',
		array (
			'type'		=> 'text',
		)
	);
	$acf_cf_opt->add_field('amocrm', 'apikey', 'APIKey:',
		'',
		array (
			'type'		=> 'text',
			'class'		=> 'regular-text',
		)
	);



	$acf_cf_opt->init();

	$options = $acf_cf_opt->value();

?>