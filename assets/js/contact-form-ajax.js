jQuery(function($) {
	$(function() {
		$('.contact_form_button_popup').fancybox({
			padding: 0,
			hideOnOverlayClick: true,
		});

		$('.contact_form form').on('submit', function(event) {
			event.preventDefault();
			var $form = $(this);
			$.fancybox.showActivity();
			$.ajax({
				url: $form.attr('action') || contact_form_ajax_url,
				type: $form.attr('method') || 'post',
				data: $form.serializeArray(),
				dataType: 'json',
				success: function(response) {
					console.log(response);
					$.fancybox.hideActivity();
					if (response.status == 'success') {
						$form.trigger('reset');
						$.fancybox(response.message);
						var group = $form.find('[name=goal_group]').val() || 'form';
						var goal = $form.find('[name=goal_name]').val() || 'goal_order';
						if (goal) {
							if (typeof(yaCounter) != 'undefined') {
								yaCounter.reachGoal(goal);
								console.info('send Yandex action: ' + goal);
							}
							if (typeof(GoogleAnalyticsObject) != 'undefined') {
								ga('send', 'event', group, goal);
								console.info('send Google Analytics action: ' + group + ', ' + goal);
							}
						}
					} else {
						$responce = $form.closest('.contact_form').find('.contact_form_error');
						$responce.html(response.message);
						$responce.show();
						window.setTimeout(function() {
							$responce.hide();
						}, 3000);
					}
				}
			});
		});

		$('.contact_form_close').on('click', function(event) {
			event.preventDefault();
			$.fancybox.close();
		});
	});
});